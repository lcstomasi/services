<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Field extends Model
{
	protected $table    = 'fields';
    protected $fillabel = [ 'name',
						    'label',
						    'ref_field_type',
						    'height',
						    'width',
						    'sql', ];
}
