<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => 'web'], function () {
    
    // LOGIN
    Route::auth();
    
    // FIELDS
	Route::get('/fields' , 'FieldController@list');
	Route::get('/fields/new' , 'FieldController@new');
	Route::get('/fields/edit/{id}' , 'FieldController@edit');
	Route::get('/fields/delete/{id}' , 'FieldController@delete');
	Route::post('/fields/onSave' , 'FieldController@onSave');
	
	// HOME
    Route::get('/home', 'HomeController@index');
});
