<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Field;
use App\FieldType;


class FieldController extends Controller
{
	    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

	public function list( Request $request )
	{
		$fields = Field::paginate(5);
		
		if( $request->ajax() )
		{
			return view( 'fields.list' , [ 'fields' => $fields ] )->renderSections();
		}
		else
		{
			return view( 'fields.list' , [ 'fields' => $fields ] );
		}
	}

	public function new( Request $request )
	{
		$field = (object)null;
		$field->id     = null;
		$field->name   = null;
		$field->label  = null;
		$field->width  = null;
		$field->height = null;
		$field->sql    = null;
		$field->ref_field_type = null;

		$field->fields_types = FieldType::getArrayComposer();

		if( $request->ajax() )
		{
			return view( 'fields.form' , [ 'field' => $field ] )->renderSections();
		}
		else
		{
			return view( 'fields.form' , [ 'field' => $field ] );	
		}
	}

	public function onSave( Request $request )
	{
        $inputs = $request->all();
        try 
        {
			if( !empty($inputs['id']) )
			{
			    $field = Field::find( $inputs['id'] );
			} 
			else
			{	
			    $field = new Field();
			}
				
			$field->name   		   = $inputs['name'];
			$field->label  		   = $inputs['label'];
			$field->width  		   = $inputs['width'];
			$field->height 		   = $inputs['height'];
			$field->sql    		   = $inputs['sql'];
			$field->ref_field_type = $inputs['ref_field_type'];

			$field->save();

			return redirect("/fields/edit/$field->id");
       	
        } 
        catch (\Exception $e) 
        {
        	echo "<script>
        			alerta();
        			</script>";
        }
	}

	public function edit( $id )
	{
		$field = Field::find($id);
		$field->fields_types = FieldType::getArrayComposer();

		return view( 'fields.form' , [ 'field' => $field ] );	
	}

	public function delete( $id )
	{
		$field = Field::find( $id );
		$field->delete();

		return $this->list();
	}
}
