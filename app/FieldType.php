<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FieldType extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'fields_types';

    protected $fillable = [
        'name',
    ];

    public static function getArrayComposer()
    {
    	$types = FieldType::orderBy('id','asc')->get();

    	$arrayComposer = array();
    	
    	foreach ($types as $type) 
    	{
    		$arrayComposer[ $type->id ] = $type->name;
    	}
    	return $arrayComposer;
    }

    public static function getName( $id )
    {
    	return FieldType::find($id)->name;
    }
}