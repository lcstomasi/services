function message( message, title , action )
{
	bootbox.dialog({
		  message: message,
		  title: title,
		  buttons: {
		    success: {
		      label: 'Ok',
		      className: 'btn-success',
		      callback: function() {
		        window.location.href = action;
		      }
		    },
		    danger: {
		      label: 'Cancel',
		      className: 'btn-danger',
		      callback: function() {
		        $('.modal-dialog').fadeOut('slow');
		      }
		    }
		  }
		});
}

function redirect( local ){
    if( local != '' ){ 
        event.preventDefault();
        $.get( local , function( data ) {
            $( "#content" ).html( data.content );
        });
        window.history.pushState( {}, 'Services', local );
    }
}

function alerta()
{
	$('.panel-footer').parent().append('<div>Hello Word!</div>');
}