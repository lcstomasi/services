<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Fields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if( !Schema::hasTable('fields') )
        {
            Schema::create('fields', function (Blueprint $table) {
                $table->increments('id');
                $table->string('name'  , 255)->unique()->nullable();
                $table->string('label' , 255)->nullable();
                $table->integer('ref_field_type')->foreign('ref_field_type_fields')->references('id')->on('fields_types');
                $table->integer('height');
                $table->integer('width')->nullable();
                $table->text('sql')->nullable();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('fields');
    }
}
