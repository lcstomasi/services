@extends('layouts.app')

@section('content')
<body>
	<script src="//code.jquery.com/jquery-1.12.0.min.js"></script>
        <ul>
            <li id='oi' class="items">
                <i class="fa fa-user"></i>
                <h3>Usuários</h3>
                <p>Ferramenta para cadastrar, editar, excluir e visualizar os usuários do sistema.</p>
            </li>
            <li id="t" style="display:none;">
                <i class="fa fa-building" ></i>
                <h3>Entidades</h3>
                <p>Ferramenta para cadastrar, editar, excluir e visualizar as empresas que serão utilizadas na manutenção dos lançamentos.</p>
            </li>
            
            <li class="items" onclick="application.show( 2 )" >
                <i class="fa fa-check"></i>
                <h3>Tipos de Finalizações</h3>
                <p>Ferramenta para cadastrar, editar, excluir e visualizar os tipos de finalizações que serão utilizados ao encerrar um lançamentos.</p>
            </li>
            
            <li class="items" onclick="application.show( 3 )" >
                <i class="fa fa-sitemap"></i>
                <h3>Categorias Lançamentos</h3>
                <p>Ferramenta para cadastrar, editar, excluir e visualizar as categorias que serão utilizados nos lançamentos, com a finalidade de organizar e filrar os lançamentos.</p>
            </li>
            
            <li class="items" onclick="application.show( 4 )" >
                <i class="fa fa-code-fork"></i>
                <h3>Tipos Lançamentos</h3>
                <p>Ferramenta para cadastrar, editar, excluir e visualizar os tipos que serão utilizados nas categorias de lançamentos, com a finalidade de organizar e filrar as categorias.</p>
            </li>
        </ul>
    Your Application's Landing Page.
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$('#oi').click( function(){
			if( $('#t').css('display') == 'none' )
			{
				$('#t').slideDown( 500 );
			}
			else
			{
				$('#t').slideUp( 500 );
			}
		});
	});
</script>
@endsection
