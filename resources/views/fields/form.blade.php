@extends('layouts.app')

@section('content')
<div id = 'content'>
	<div class="panel-heading title blue">Fields</div>
	<div class="content">
		{!! Form::open(array('url'=>'/fields/onSave', 'method'=>'POST' )) !!}
			{!! Form::hidden('id' , "{$field->id}" )!!}	
			<div class="form-inline  form-group">
				{!! Form::label('label_label', 'Label' , array( 'class' => 'form_label') )!!}	
				{!! Form::text('label' , "{$field->label}" , array( 'class' => 'form-control form_input' ) )!!}	
			</div>
			<div class="form-inline  form-group">
				{!! Form::label('label_name', 'Name' , array( 'class' => 'form_label') )!!}	
				{!! Form::text('name' , "{$field->name}" , array( 'class' => 'form-control form_input' ) )!!}	
			</div>
			<div class="form-inline  form-group">
				{!! Form::label('label_height', 'Height' , array( 'class' => 'form_label') )!!}	
				{!! Form::text('height' , "{$field->height}" , array( 'class' => 'form-control form_input' ) )!!}	
			</div>
			<div class="form-inline  form-group">
				{!! Form::label('label_width', 'Width' , array( 'class' => 'form_label') )!!}	
				{!! Form::text('width' , "{$field->width}" , array( 'class' => 'form-control form_input' ) )!!}	
			</div>
			<div class="form-inline  form-group">
				{!! Form::label('label_ref_field_type', 'Field Type' , array( 'class' => 'form_label') ) !!}	
				{!! Form::select('ref_field_type' , $field->fields_types , "{$field->ref_field_type}" , array( 'class' => 'form-control form_input' ) ) !!}	
			</div>
			<div class="form-inline  form-group">
				{!! Form::label('label_sql', 'SQL' , array( 'class' => 'form_label') )!!}	
				{!! Form::textarea('sql' , "{$field->sql}" , array( 'class' => 'form-control form_input' ) )!!}	
			</div>
	</div>
	<div class="panel-footer">
		{!! Form::submit('Save' , array('class'=>'btn btn-default'))!!}
		{!! Form::button('New'  , array( 'class'=>'btn btn-success' , 'onClick' => "redirect('/fields/new');" ))!!}
		{!! Form::button('Back to List' , array( 'class'=>'btn btn-warning' , 'onClick' => "redirect('/fields');" ))!!}
	</div>
	{!! Form::close()!!}
</div>
@endsection