<?php  use App\FieldType; ?>

@extends('layouts.app')

@section('content')
<div id = 'content'>
	<div class="panel-heading title blue">Fields</div>
	<div class="content">
		<table class="table table-hover">
			<thead><td>#</td><td>Label</td><td>Type</td><td class="btn-list">&nbsp;</td><td class="btn-list">&nbsp;</td></thead>
			@foreach( $fields as $field )
			<tr>
				<td>{{$field->id}}</td>
				<td>{{$field->label}}</td>
				<td>{{FieldType::getName($field->ref_field_type)}}</td>
				<td>{!! Form::button( 'Edit' , array('class'=>'btn btn-info' , 'onClick' => "window.location.href='/fields/edit/{$field->id}';" )) !!}</td>
				<td>{!! Form::button( 'Delete' , array('class'=>'btn btn-danger' , 'onClick' => "message( 'Confirm Deletion?' , 'Alert' , '/fields/delete/{$field->id}');" ) ) !!}</td>
			</tr>	
			@endforeach
		</table>
		<div class="center">
	    	{{ $fields->links() }}
		</div>
		<div class="panel-footer">
			{!! Form::button('New' , array( 'class'=>'btn btn-success' , 'onClick' => "redirect('/fields/new');" ))!!}
		</div>
	</div>
</div>
@endsection

