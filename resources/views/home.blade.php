@extends('layouts.app')

@section('content')

<div class="panel-heading title blue">Home</div>
    <div class="panel panel-default">
        <div class="panel-heading"><i class="fa fa-circle blue">&nbsp;</i>Registrations</div>
        <div class="center">
            <a href="#" class="btn btn-default menu blue" role="button">Services</a>
            <a href="#" class="btn btn-default menu blue" role="button">Physical Person</a>
            <a href="#" class="btn btn-default menu blue" role="button">Legal Person</a>
            <a href="/fields" class="btn btn-default menu blue" role="button">Fields</a>
            <a href="#" class="btn btn-default menu blue" role="button">Routes</a>
        </div>
</div>
       
@endsection
